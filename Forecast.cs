﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HappyWeather
{
    public class Forecast
    {
        public List<VremePodatak> today;
        public List<VremePodatak> tomorrow;
        public List<VremePodatak> fiveDay;

        public Forecast(City city)
        {
            
            today = new List<VremePodatak>();
            tomorrow = new List<VremePodatak>();
            fiveDay = new List<VremePodatak>();

            //call 1
            string URL = "https://api.openweathermap.org/data/2.5/onecall";
            string urlParameters = "?lat=" + city.coord.lat + "&lon=" + city.coord.lon + "&appid=63c4eec6e201390b7cf298a37af8af1b&units=metric";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;

            string res = response.Content.ReadAsStringAsync().Result;
            JsonForecast o = JsonConvert.DeserializeObject<JsonForecast>(res);

            int todayint = VremePodatak.UnixTimeStampToDateTime(o.hourly[0].dt).Day;
            int tomorrowint = todayint + 1;
            foreach(var datapoint in o.hourly)
            {
                if(todayint == VremePodatak.UnixTimeStampToDateTime(datapoint.dt).Day)
                {
                    today.Add(new VremePodatak(city, datapoint));
                }
                else if(tomorrowint == VremePodatak.UnixTimeStampToDateTime(datapoint.dt).Day)
                {
                    tomorrow.Add(new VremePodatak(city, datapoint));
                }
            }

            //call 2
            URL = "https://api.openweathermap.org/data/2.5/forecast";
            urlParameters = "?id=" + city.id + "&appid=63c4eec6e201390b7cf298a37af8af1b&units=metric";
            HttpClient clienttwo = new HttpClient(); 
            clienttwo.BaseAddress = new Uri(URL);
            response = clienttwo.GetAsync(urlParameters).Result;

            res = response.Content.ReadAsStringAsync().Result;
            JsonForecastTwo otwo = JsonConvert.DeserializeObject<JsonForecastTwo>(res);
            Console.WriteLine();
            foreach(var datapoint in otwo.list)
            {
                fiveDay.Add(new VremePodatak(city, datapoint));
            }
        }


    }

    
    //classes for json parsing type 1
    public class JsonForecast
    {
        public List<DataPoint> hourly;
    }

    public class DataPoint
    {
        public long dt;
        public double temp;
        public double pressure;
        public double humidity;
        public double clouds;
    }

    //classes for json parsin type 2
    
    public class JsonForecastTwo
    {
        public List<DataPointTwo> list;
    }

    public class DataPointTwo
    {
        public long dt;
        public DataPointMain main;
        public DataPointClouds clouds;
    }
    public class DataPointMain
    {
        public double temp;
        public double pressure;
        public double humidity;
    }

    public class DataPointClouds
    {
        public double all;
    }

    //class used for representation
    public class VremePodatak
    {
        public VremePodatak(City city, DataPointTwo dp)
        {
            Grad = city;
            NazivGrada = city.name;
            VremeMerenja = UnixTimeStampToDateTime(dp.dt);
            Temperatura = dp.main.temp;
            VazdusniPritisak = dp.main.pressure;
            Oblaci = dp.clouds.all;
            Vlaznost = dp.main.humidity;

        }
        public VremePodatak(City city, DataPoint dp)
        {
            Grad = city;
            NazivGrada = city.name;
            VremeMerenja = UnixTimeStampToDateTime(dp.dt);
            Temperatura = dp.temp;
            VazdusniPritisak = dp.pressure;
            Oblaci = dp.clouds;
            Vlaznost = dp.humidity;
        }
        public City Grad { get; set; }
        public string NazivGrada { get; set; }
        public DateTime VremeMerenja { get; set; }
        public double Temperatura { get; set; }
        public double VazdusniPritisak { get; set; }
        public double Oblaci { get; set; }
        public double Vlaznost { get; set; }

        public static DateTime UnixTimeStampToDateTime(long dt)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dateTime.AddSeconds(dt).ToLocalTime();
        }
    }
}
