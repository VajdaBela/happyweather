﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json;
using LiveCharts;
using LiveCharts.Wpf;
using System.Windows.Media;
using System.Linq;

namespace HappyWeather
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public ObservableCollection<string> Labels { get; set; }

        public List<City> Allcities;
        public ObservableCollection<City> filteredCities { get; set; }
        public ObservableCollection<City> selectedCities { get; set; }
        public ChartValues<double> graphValues { get; set; }

        public ObservableCollection<VremePodatak> vremena;

        public typeOfValues graphdata;
        public typeOfTime graphtime;

        public Dictionary<string, LineSeries> graphLineSeries;
        public Dictionary<string, ChartValues<double>> graphChartValues;
        public Dictionary<string, Forecast> graphForecast;

        public Separator defaultSeparator;
        
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            filteredCities = new ObservableCollection<City>();
            filteredCities.Add(new City { name = "Novi Sad" });
            filteredCities.Add(new City { name = "Beograd" });

            graphValues = new ChartValues<double>();

            selectedCities = new ObservableCollection<City>();

            vremena = new ObservableCollection<VremePodatak>();

            graphdata = typeOfValues.Nothing;
            graphtime = typeOfTime.Noting;

            try { 
                ReadCities();
            }
            catch(Exception e)
            {
                MessageBox.Show("Nisu se mogli učitati gradovi! Program će se zatvoriti.", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Windows.Application.Current.Shutdown();
                return;
            }
            bestCities();
            MainDataGrid.ItemsSource = vremena;

            graphLineSeries = new Dictionary<string, LineSeries>();
            graphChartValues = new Dictionary<string, ChartValues<double>>();
            graphForecast = new Dictionary<string, Forecast>();

            Labels = new ObservableCollection<string>();

            defaultSeparator = xAxis.Separator;
            
        }

        public void updateFilteredListBox()
        {
            filteredCities.Clear();
            string searchString = SearchTextBox.Text;
            if (searchString.Equals(""))
            {
                bestCities();
            }
            int numberAdded = 0;
            foreach (City city in Allcities)
            {
                if (city.name.Equals("-"))
                {
                    continue;
                }
                if (city.name.ToLower().StartsWith(searchString.ToLower()))
                {
                    filteredCities.Add(city);
                    numberAdded += 1;
                }
                if (numberAdded >= 100)
                {
                    break;
                }
            }
        }

        public void bestCities()
        {
            filteredCities.Clear();
            using (StreamReader cityFile = new StreamReader("..\\..\\list.txt"))
            {
                string cityName = "";
                while ((cityName = cityFile.ReadLine()) != null)
                {
                    filteredCities.Add(FindCity(cityName));
                }
            }
        }

        public City FindCity(string name)
        {
            foreach (City city in Allcities)
            {
                if (city.name == name)
                {
                    return city;
                }
            }
            return null;
        }

        private void ReadCities()
        {
            WebClient wb = new WebClient();
            wb.DownloadFile("http://bulk.openweathermap.org/sample/city.list.json.gz", Path.GetTempPath() + "city.list.json.gz");
            using (FileStream file = new FileStream(Path.GetTempPath() + "city.list.json.gz", FileMode.Open))
            using (GZipStream decompresor = new GZipStream(file, CompressionMode.Decompress))
            using (StreamReader jsonText = new StreamReader(decompresor, Encoding.UTF8))
            {
                JsonSerializer serializer = new JsonSerializer();
                Allcities = (List<City>)serializer.Deserialize(jsonText, typeof(List<City>));
            }
        }

        private void SearchTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            updateFilteredListBox();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            City city = (City)FilteredListBox.SelectedItem;


            try
            {
                graphForecast[city.name] = new Forecast(city);
            }
            catch (Exception except)
            {
                MessageBox.Show("Nisu se mogli učitati podaci za " + city.name + "!", "Greska" ,MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            selectedCities.Add(city);
            filteredCities.Remove(city);
            graphLineSeries[city.name] = new LineSeries();
            graphLineSeries[city.name].Title = city.name;
            graphLineSeries[city.name].Fill = System.Windows.Media.Brushes.Transparent;
            graphChartValues[city.name] = new ChartValues<double>();
            graphLineSeries[city.name].Values = graphChartValues[city.name];
            MainCartesionChart.Series.Add(graphLineSeries[city.name]);
            updateGraph();
            updateTable();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            City city = (City)SelectedListBox.SelectedItem;
            selectedCities.Remove(city);
            updateFilteredListBox();

            MainCartesionChart.Series.Remove(graphLineSeries[city.name]);
            graphForecast.Remove(city.name);
            graphLineSeries.Remove(city.name);
            graphChartValues.Remove(city.name);
            updateGraph();
            updateTable();
        }

        public void updateTable()
        {
            List<VremePodatak> vremena = new List<VremePodatak>();
            this.vremena.Clear();
            foreach(City city in SelectedListBox.Items)
            { 
                switch (graphtime)
                {
                    case typeOfTime.Today:
                        xAxis.Separator = defaultSeparator;
                        vremena.AddRange(graphForecast[city.name].today);
                        break;
                    case typeOfTime.Tomorrow:
                        xAxis.Separator = defaultSeparator;
                        vremena.AddRange(graphForecast[city.name].tomorrow);
                        break;
                    case typeOfTime.FiveDay:
                        xAxis.Separator = new Separator { Step = (int)(MainCartesionChart.ActualWidth / 62) };
                        vremena.AddRange(graphForecast[city.name].fiveDay);
                        break;
                    default:
                        vremena = new List<VremePodatak>();
                        break;
                }
                
            }
            vremena.Sort(vremecom);
            foreach (VremePodatak vreme in vremena)
            {
                this.vremena.Add(vreme);
            }

        }

        public static int vremecom(VremePodatak x, VremePodatak y)
        {
            return x.VremeMerenja >= y.VremeMerenja ? 1 : -1;
        }
        public void updateGraph()
        {
            Labels.Clear();
            foreach (City city in SelectedListBox.Items)
            {
                graphChartValues[city.name].Clear();
                List<VremePodatak> vremena = null;
                
                switch(graphtime)
                {
                    case typeOfTime.Today:
                        xAxis.Separator = defaultSeparator;
                        vremena = graphForecast[city.name].today;
                        break;
                    case typeOfTime.Tomorrow:
                        xAxis.Separator = defaultSeparator;
                        vremena = graphForecast[city.name].tomorrow;
                        break;
                    case typeOfTime.FiveDay:
                        xAxis.Separator = new Separator { Step =(int)(MainCartesionChart.ActualWidth / 62) };
                        vremena = graphForecast[city.name].fiveDay;
                        break;
                    default:
                        vremena = new List<VremePodatak>();
                        break;
                }
                
                foreach(VremePodatak vreme in vremena)
                {
                    if(graphtime != typeOfTime.FiveDay)
                    {
                        Labels.Add(vreme.VremeMerenja.Hour.ToString()+":00");
                    }
                    else
                    {
                        Labels.Add(vreme.VremeMerenja.Day.ToString() + "." + vreme.VremeMerenja.Month.ToString() + ". " + vreme.VremeMerenja.Hour+":00");
                    }
                    
                    switch(graphdata)
                    {
                        case typeOfValues.Temperatura:
                            graphChartValues[city.name].Add(vreme.Temperatura);
                            break;
                        case typeOfValues.Vlaznost:
                            graphChartValues[city.name].Add(vreme.Vlaznost);
                            break;
                        case typeOfValues.VazdusniPritisak:
                            graphChartValues[city.name].Add(vreme.VazdusniPritisak);
                            break;
                        case typeOfValues.Oblacnost:
                            graphChartValues[city.name].Add(vreme.Oblaci);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            graphtime = typeOfTime.Today;
            updateGraph();
            updateTable();
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            graphtime = typeOfTime.Tomorrow;
            updateGraph();
            updateTable();
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            graphtime = typeOfTime.FiveDay;
            updateGraph();
            updateTable();
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {

            MainCartesionChart.AxisY.Clear();
            MainCartesionChart.AxisY.Add(new Axis { Title = "temperatura °C" });
            graphdata = typeOfValues.Temperatura;
            updateGraph();
        }
        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            MainCartesionChart.AxisY.Clear();
            MainCartesionChart.AxisY.Add(new Axis { Title = "vazdušni pritisak hPa" });
            graphdata = typeOfValues.VazdusniPritisak;
            updateGraph();
        }
        private void RadioButton_Checked_5(object sender, RoutedEventArgs e)
        {
            MainCartesionChart.AxisY.Clear();
            MainCartesionChart.AxisY.Add(new Axis { Title = "oblačnost %" });
            graphdata = typeOfValues.Oblacnost;
            updateGraph();
        }
        private void RadioButton_Checked_6(object sender, RoutedEventArgs e)
        {
            MainCartesionChart.AxisY.Clear();
            MainCartesionChart.AxisY.Add(new Axis { Title = "vlažnost vazduha %" });
            graphdata = typeOfValues.Vlaznost;
            updateGraph();
        }
    }

    public enum typeOfValues
    {
        Nothing,
        Temperatura,
        VazdusniPritisak,
        Oblacnost,
        Vlaznost
    }

    public enum typeOfTime
    {
        Noting,
        Today,
        Tomorrow,
        FiveDay
    }

    public class City
    {
        public string id;
        public string name;
        public string state;
        public string country;
        public Coordinate coord;

        public override string ToString()
        {
            return name;
        }

        public static int CompareCity(City x, City y)
        {
            return x.name.ToLower().CompareTo(y.name.ToLower());
        }
    }

    public class Coordinate
    {
        public string lon;
        public string lat;
    }
}
